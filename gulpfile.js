'use strict';

const { task,series } = require('gulp');
const { src, dest } = require('gulp');
const babel = require('gulp-babel');
const htmlmin = require('gulp-htmlmin');
const sass = require('gulp-sass');
const image = require('gulp-image');
var sourcemaps = require('gulp-sourcemaps');

sass.compiler = require('node-sass');

task('build_html', function(cb) {
    build_html();
    cb();
});
task('build_js', function(cb) {
    build_js();
    cb();
});
task('build_php', function(cb) {
    build_php();
    cb();
});
task('build_sass', function(cb) {
    build_sass();
    cb();
});
task('build_images', function(cb) {
    build_images();
    cb();
});



function build_html() {
    return src('src/**/*.html')
      .pipe(htmlmin({ collapseWhitespace: true, removeComments: true }))
      .pipe(dest('dist/'));
}

function build_js() {
    return src('src/**/*.js')
    .pipe(babel({presets: ['@babel/env']}))
    .pipe(dest('dist/'));
}

function build_php() {
    return src('src/**/*.php')
    .pipe(dest('dist/'));
}

function build_sass() {
    return src('src/**/style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(dest('dist'));
}

function build_images() {
    return src('src/**/*.png')
    .pipe(image())
    .pipe(dest('dist'));
}


function build_all(cb) {
    build_html();
    build_js();
    build_php();
    build_sass();
    cb();
}

exports.build = build_all;
exports.default = series(build_all);